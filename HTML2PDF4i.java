
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.DocumentException;

public class HTML2PDF4i {
    
    public static void main(String[] args) 
            throws IOException, DocumentException {
    	
        String inputFile = args[0];
        String url = new File(inputFile).toURI().toURL().toString();
        String outputFile = args[1];
        OutputStream os = new FileOutputStream(outputFile);
        
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocument(url);
        renderer.layout();
        renderer.createPDF(os);
        
        os.close();
    }
}
