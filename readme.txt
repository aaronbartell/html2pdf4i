
---------------------------------------------------------------------------------------------------
                                       HTML2PDF4i v1.0b

                              product of www.MowYourLawn.com
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
                                       Installation
---------------------------------------------------------------------------------------------------


1. Unzip the downloaded file to c:\temp (or the directory of your choice).

2. Upload RPG source members HTML2PDF4I and T_H2P4I to the IBM i into a source physical file and
   compile them - HTML2PDF4I first and then T_H2P4I.

3. Create IFS directory /java/html2pdf4i with the following commands.

     MKDIR '/java'
     MKDIR '/java/html2pdf4i'

4. Upload the following files to IFS folder /java/html2pdf4ibmi.  The files can be found in the 
   zip file that was downloaded.

     core-renderer-minimal.jar
     core-renderer.jar
     HTML2PDF4i.jar
     iText-2.0.8.jar
     music.html
     xml-apis-xerces-2.9.1.jar

5. Invoke the test program as follows to run a test.

     CALL T_H2P4I

6. Look in the IFS to see if the conversion took place successfully using the following:

     WRKLNK '/java/html2pdf4i/*'
   
7. To view the created PDF you will need to download it onto your PC. To do this you will need to 
   use FTP, a Windows share, WDSC, or other method.


---------------------------------------------------------------------------------------------------
                                      Requirements
---------------------------------------------------------------------------------------------------

- Need to have JDK 1.5 (aka 5.0) installed on your IBM i

---------------------------------------------------------------------------------------------------
                                      Contact
---------------------------------------------------------------------------------------------------

To contact me, Aaron Bartell, please shoot an email to aaronbartell@gmail.com

---------------------------------------------------------------------------------------------------
                                     Updates
---------------------------------------------------------------------------------------------------

v1.00  - Original

